# Problema de resta de palíndromos

En este problema lo que hay que sacar es el número de palíndromos entre dos números dados. La dificultad está en que los números admitidos son hasta $`10^{18}`$ , por lo que la cosa no es tan simple como calcular palíndromos a lo bruto.

Por eso, he intentado simplificarlo un poco con un modelo matemático: calcular los palíndromos que hay de `digitos` cifras.

Así, cuando un número sea de una cifra, la forma que tendrán estos palíndromos es de $`n`$, por lo que habrá un total de 9 palíndromos de una cifra, o lo que es lo mismo, de 1 a 9. Para un número de dos cifras, el palíndromo tendrá la forma $`nn`$, por lo que habrá también 9 palíndromos entre 10 y 100.

Sin embargo, cuando el número tiene 3 cifras, adota la forma $`nmn`$, siendo $`m`$ un número comprendido entre el 0 y el 9 (10 posibles números), y n un número del 1 al 9 (porque no hay números naturales que empiecen por 0), por lo que el número de palíndromos en un número de 3 cifras (del 100 al 999) es de $`9 \cdot 10 = 90`$. Igual para los números de 4 cifras, que tendrían la forma $`nmmn`$.

Aquí va una tabla para que más o menos lo visualicéis:

| Rango | Forma | Número de capicúas |
|:---:|:---:|:---:|
| $`0 < x < 10`$ | $`n`$  | $`n = 9`$ |
| $`10 < x < 100`$ | $`nn`$ | $`n = 9`$ |
| $`100 < x < 1000`$ | $`nmn`$  | $`n \cdot m = 9 \cdot 10 = 90`$  |
| $`1000 < x < 10000`$ | $`nmmn`$  | $`n \cdot m = 9 \cdot 10 = 90`$  |
| $`10000 < x < 100000`$ | $`nmomn`$  | $`n \cdot m \cdot o = 9 \cdot 10 \cdot 10 = 900`$  |

Como véis, el número de capicúas desde una potencia de 10 a la siguiente viene dado por la función $` 9 \cdot 10 ^ {digitos - 1 \over 2} `$. Creo que el código que llevo se entiende más o menos bien, así que a ver si se os ocurre algo para arreglarme las funciones que no van
