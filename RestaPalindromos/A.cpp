#include <iostream>
#include <math.h>

using namespace std;

long long int inverso(long long int m) {
    long long int reversoFinal = 0;
    int resto;
    while (m) {
        resto = m % 10;
        reversoFinal = (10 * reversoFinal) + resto;
        m = m / 10;
    }
    return reversoFinal;
}

int palindromosHasta(long long int n) {
    int pal = 0, digitos = 0, ndigit;
    long long int j, l, numeroReducido = n;

    do {
        digitos++;
        ndigit /= 10;
    } while (ndigit);

    int digitosReducidos = digitos;

    while (numeroReducido > 10) {
        numeroReducido = numeroReducido / 10;
        pal = pal + (9 * pow(10, (digitos - digitosReducidos) / 2));
        digitosReducidos--;
    }

    l = pow(10, (digitos-1));

    for (j = 1; j == n; j++) {
        int k = inverso(j);
        if (k == j) {
            pal++;
            cout << k << "\n";

        }
    }

    return pal;
}

int main() {
    long long int num1, num2, numAux;
    int palindromosTotales;

    cout << "Introduce los dos números \n";
    cin >> num1 >> num2;
    if (num1 > num2) {
        numAux = num1;
        num1 = num2;
        num2 = numAux;
    }

    palindromosTotales = palindromosHasta(num2) - palindromosHasta(num1);
    cout << palindromosTotales;

}
